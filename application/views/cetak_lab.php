<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Lab</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>

   <?php
    $kode = str_replace(' ', '-', $cl_rm);
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>
</head>
<body>
	<div style="font-size: 10px; font-family: Arial, Helvetica, sans-serif;">
		<p style="transform: rotate(-90deg);top: 48px; left: -22px; position: absolute;"><b>
					<?=date('d-m-Y');?>
					<?=date('H.i');?>
				</b>
			</p>
	<table style="width: 100%; margin-top: 3px">
		<tr>
			<td rowspan="7" width="35px">
			</td>
		</tr>
		<tr>
			 <td><center><b><?=$cl_resep;?></b></center></td> 
		</tr>
		<tr>
			<td colspan="3">
				<center><img src="<?php echo base_url();?>barcode/<?php echo $cl_resep;?>.png" style="width: 130px;height: 30px">
				</center>
			</td>
		</tr>
		<tr>
			<td style="margin: 0px; padding: 0">
				<center><b>
				<?=SUBSTR($cl_nama, 0, 20);?></b>
				</center></td>
		</tr>
		<tr>
			<td style="margin: 0px; padding: 0"><center><b><?=$cl_rm;?>/<?=$cl_umur;?>/<?=$cl_nourut;?></b>
			</center></td>
		</tr>
		<tr>
			<td style="margin: 0px; padding: 0"><center><b><?=tanggal_format($cl_tgllahir);?> / <?=$cl_ruang;?></b>
			</center></td>
		</tr>
		<tr>
			<td style="margin: 0px; padding: 0"><center><b><?=$cl_ujilab;?></b>
			</center></td>
		</tr>
	</table>
</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>
<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script>
 -->

<!-- Langsung Cetak -- Cuman Butuh Waktu untuk generate-->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
