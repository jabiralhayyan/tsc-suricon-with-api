<!-- start:Left Menu -->
<div id="left-menu">
	<div class="sub-left-menu scroll">
		<ul class="nav nav-list">
			<li><div class="left-bg"></div></li>
			<li class="time">
				<h1 class="animated fadeInLeft">21:00</h1>
				<p class="animated fadeInRight">Sat,October 1st 2029</p>
			</li>
			<li class="ripple">
				<a href="<?php echo base_url();?>home" class="nav-header">
					<span class="fa fa-home"></span> HOME </span>
				</a>
				<a href="<?php echo base_url();?>gelang/gelang_anak" class="nav-header">
					<span class="fa fa-user"></span> GELANG PASIEN </span>
				</a>
				<a href="<?php echo base_url();?>farmasi" class="nav-header">
					<span class="fa fa-user"></span> FARMASI </span>
				</a>
			</li>
		</ul>
	</div>
</div>
<!-- end: Left Menu
