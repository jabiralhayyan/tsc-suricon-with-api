<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Gelang Dewasa</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>

  <?php
	$kode = str_replace(' ', '-', $gd_rm);
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>

</head>
<body>
	<div style="font-size: 12px; font-family: Arial, Helvetica, sans-serif;">
	<table style="width: 100%; margin-top: 5px" border="0">
		<tr>
			<td rowspan="4" width="60px">
				&nbsp;&nbsp;<img src="<?=base_url();?>assets/img/logo-basoeni-new.png">
			</td>
		</tr>
		<tr>
			<td style="width:90px"><b>&nbsp;&nbsp; Nama</b></td>
			<td style="width:1px"><b>:&nbsp;</b></font></td>
			<td><b><?=SUBSTR($gd_nama, 0, 30);?>, <?=$gd_title;?></b></td>
		</tr>
		<tr>
			<td><b>&nbsp;&nbsp; Rekam Medis</b></td>
			<td><b>:&nbsp;</b></td>
			<td><b><?=$gd_rm;?></b></td>
		</tr>
		<tr>
			<td><b>&nbsp;&nbsp; Tgl Lahir</b></td>
			<td><b>:&nbsp;</b></td>
			<td><b><?=tanggal_format($gd_ttl)?></b></td>
		</tr>
	</table>
</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>

<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script> -->

<!-- Langsung Cetak -->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
