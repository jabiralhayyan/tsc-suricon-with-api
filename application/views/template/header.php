<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="author" content="BrainFreeze">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$title?></title>
	
	<!-- start: Css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">

	<!-- plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/simple-line-icons.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/animate.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/datatables.bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/fullcalendar.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/alertify.core.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/alertify.default.css" id="toggleCSS" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/loader.css"/>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/nouislider.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/select2.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/ionrangeslider/ion.rangeSlider.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/ionrangeslider/ion.rangeSlider.skinFlat.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/bootstrap-material-datetimepicker.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/plugins/summernote.css"/>
	<!-- end: Css -->

	<!-- start: Javascript -->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.ui.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
	<style>
	.alertify-log-custom {
		background: blue;
	}
</style>
<link rel="shortcut icon" href="<?=base_url();?>assets/img/favicon-basoeni.jpg">
</head>

<body id="mimin" class="dashboard">
	<!-- start: Header -->
	<nav class="navbar navbar-default header navbar-fixed-top">
		<div class="col-md-12 nav-wrapper">
			<div class="navbar-header" style="width:100%;">
				<div class="opener-left-menu is-open">
					<span class="top"></span>
					<span class="middle"></span>
					<span class="bottom"></span>
				</div>
				<a href="home" class="navbar-brand"> 
					<b>Rumah Sakit RA Basoeni</b>
				</a>
			</div>
		</div>
	</nav>
	<!-- end: Header -->

	<!-- Loader -->
	<div class="modal fade" id="loader" data-backdrop="static">
		<div class='loader'>
			<div class='circle'></div>
			<div class='circle'></div>
			<div class='circle'></div>
			<div class='circle'></div>
			<div class='circle'></div>
		</div>
		<div align="center">
			<font size="5%" color="white">Harap Tunggu.....</font>
		</div>
	</div>
