
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Farmasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->API = 'http://masterdata.unotech.id/api/api_bartenderlabel/';
	}

	public function index()
	{
		$data['title'] = 'Etiket Obat | Tablet';
		$data['query'] = '';
		$data['query_yudidi'] = '';
		//nav
		$data['nav_et_active'] = 'active';
		$data['nav_yd_active'] = '';
		$data['nav_bt_active'] = '';
		//tab
		$data['tab_et_active'] = 'in active';
		$data['tab_yd_active'] = '';
		$data['tab_bt_active'] = '';

		$data_obat = [];
		$cariresep = $this->input->post('cariresep');
		if ($cariresep != '') {
			
			  //$json_resep = '{"status":true,"data":{"no_resep":"57363","no_rm":"5736312","non_racik":[{"nama_obat":"1111111111111111111111111111 nama obat panjang sampe 2 baris bagaima","aturan":"3 x 1","keterangan":"Sebelum Makan", "qty":"15"},{"nama_obat":"Acyclovir 400 mg Indofarma","aturan":"3 x 1","keterangan":"Sebelum Makan","qty":"30"}],"racik":[{"nama_obat":"Obat Choice","aturan":"3 x 1","keterangan":"Sebelum Makan","qty":"25"},{"nama_obat":"Obat Choice 2","aturan":"3 x 1","keterangan":"Sebelum Makan","qty":"50"}]}}';
			  //$result_recipe = json_decode($json_resep);
			
			//RESEP
			$result_recipe = $this->getRecipe($cariresep);
			if(isset($result_recipe->status) && $result_recipe->status == "true") {
				$data['query'] = 'success';
				$data['response'] = $result_recipe->status;
				//Jumlah Obat Racik & Non Racik
				$count_nonracik = 0;
				$count_racik = 0;
				if (!empty($result_recipe->data->non_racik)) $count_nonracik = count($result_recipe->data->non_racik);
				if (!empty($result_recipe->data->racik)) $count_racik = count($result_recipe->data->racik);
				
				$data['jumlaharr'] = $count_nonracik+$count_racik;
				// No Resep & RM
				$data['no_resep'] = $result_recipe->data->no_resep;
				$data['no_rm'] = $result_recipe->data->no_rm;
				//Non Racik
				for ($i=0; $i<$count_nonracik; $i++) {
				 	$data['datas'][$i]['nama_obat'] = $result_recipe->data->non_racik[$i]->nama_obat;
				 	$data['datas'][$i]['aturan'] = $result_recipe->data->non_racik[$i]->aturan;
				 	$data['datas'][$i]['keterangan'] = $result_recipe->data->non_racik[$i]->keterangan;
				 	$data['datas'][$i]['qty'] = $result_recipe->data->non_racik[$i]->qty;
				 	array_push($data_obat, $data['datas'][$i]);
				}
				//Racik
				for ($i=0; $i<$count_racik; $i++) {
				 	$data['datas'][$i]['nama_obat'] = $result_recipe->data->racik[$i]->nama_obat;
				 	$data['datas'][$i]['aturan'] = $result_recipe->data->racik[$i]->aturan;
				 	$data['datas'][$i]['keterangan'] = $result_recipe->data->racik[$i]->keterangan;
				 	$data['datas'][$i]['qty'] = $result_recipe->data->racik[$i]->qty;
				 	array_push($data_obat, $data['datas'][$i]);
				}

				//$json_pasien = '{"status":true, "data":{"no_rm":"5736312","title":"Tn","nama":"Asyrofii Ambiya Nama Panjang Test Test Tes","tanggal_lahir":"1991-07-16","alamat":"Selorejo"}}';
				//$result = json_decode($json_pasien);
				//RM
				$result = $this->getPatient($data['no_rm']);
				$data['nama'] = $result->data->nama;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
			}	
		}
		
		$data['datas'] = $data_obat;
		$this->load->view('farmasi', $data);
	}

	public function test($nama){
		$data['nama'] = $nama;
		$this->load->view('test', $data);
	}

	public function yudidi()
	{
		$data['title'] = 'Yudidi | Farmasi';
		$data['query_yudidi'] = '';
		$data['query'] = '';
		$caripasien = $this->input->post('caripasien');
		if ($caripasien != '') {
			//$json_pasien = '{"status":true, "data":{"no_rm":"5736312","title":"Tn","nama":"MUHAMMAD JABIR AL HAYYAN","tanggal_lahir":"1991-07-16","alamat":"Selorejo"}}';
			//$result = json_decode($json_pasien);

			$result = $this->getPatient($caripasien);
			if(isset($result->status) && $result->status == "true") {
				$data['query_yudidi'] = 'success';
				$data['no_rm'] = $result->data->no_rm;
				$data['nama'] = $result->data->nama;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
				$data['alamat'] = $result->data->alamat;
			}
		}
		//nav
		$data['nav_et_active'] = '';
		$data['nav_yd_active'] = 'active';
		$data['nav_bt_active'] = '';
		//tab
		$data['tab_et_active'] = '';
		$data['tab_yd_active'] = 'in active';
		$data['tab_bt_active'] = '';
		$this->load->view('farmasi', $data);
	}

	public function batch()
	{
		$data['title'] = 'Batch | Farmasi';
		$data['query_yudidi'] = '';
		$data['query'] = '';
		//nav
		$data['nav_et_active'] = '';
		$data['nav_yd_active'] = '';
		$data['nav_bt_active'] = 'active';
		//tab
		$data['tab_et_active'] = '';
		$data['tab_yd_active'] = '';
		$data['tab_bt_active'] = 'in active';
		$this->load->view('farmasi', $data);
	}

	public function cetak_yudidi(){
		$data['yd_rm'] = $this->input->post('yd_rm');
		$data['yd_nama'] = $this->input->post('yd_nama');
		$data['yd_title'] = $this->input->post('yd_title');
		$data['yd_ttl'] = $this->input->post('yd_ttl');
		$data['yd_tglpemberian'] = $this->input->post('yd_tglpemberian');
		$data['yd_waktu'] = $this->input->post('yd_waktu');
		$data['yd_waktu_manual'] = $this->input->post('yd_waktu_manual');
		$data['yd_aturan'] = $this->input->post('yd_aturan');
		$data['yd_aturan_manual'] = $this->input->post('yd_aturan_manual');
		//Send API POST
		$data_api['idksolabel'] = '14';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API, $data_api, array(CURLOPT_BUFFERSIZE => 100));

		$this->load->view('cetak_yudidi', $data);
	}

	public function cetak_batch(){
		$data['bt_namaobat'] = $this->input->post('bt_namaobat');
		$data['bt_nobatch'] = $this->input->post('bt_nobatch');
		$data['bt_noregistrasi'] = $this->input->post('bt_noregistrasi');
		$data['bt_tglexpired'] = $this->input->post('bt_tglexpired');
		$data['bt_tglproduksi'] = $this->input->post('bt_tglproduksi');
		$this->generate_barcode($data['bt_nobatch']);
	
		//Send API POST
		$data_api['idksolabel'] = '13';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API, $data_api, array(CURLOPT_BUFFERSIZE => 100));
		$this->load->view('cetak_batch', $data);
	}

	public function cetak_etiketputih(){
		$data_putih = [];
		$jumlaharrputih = 0;
		$data['et_nama'] = $this->input->post('et_nama');
		$data['et_rm'] = $this->input->post('et_rm');
		$data['et_title'] = $this->input->post('et_title');
		$data['et_ttl'] = $this->input->post('et_ttl');
		$data['jumlaharr'] = $this->input->post('jumlaharr');
		for($i=0; $i<$data['jumlaharr']; $i++) {
			$data['datas'][$i]['et_namaobat'] = $this->input->post('et_namaobat'.$i);
			$data['datas'][$i]['et_jumlahobat'] = $this->input->post('et_jumlahobat'.$i);
			$data['datas'][$i]['et_aturan'] = $this->input->post('et_aturan'.$i);
			$data['datas'][$i]['et_satuan'] = $this->input->post('et_satuan'.$i);
			$data['datas'][$i]['et_satuan_manual'] = $this->input->post('et_satuan_manual'.$i);
			$data['datas'][$i]['et_takaran'] = $this->input->post('et_takaran'.$i);
			$data['datas'][$i]['et_takaran_manual'] = $this->input->post('et_takaran_manual'.$i);
			$data['datas'][$i]['et_waktuminum'] = $this->input->post('et_waktuminum'.$i);
			$data['datas'][$i]['et_waktuminum_manual'] = $this->input->post('et_waktuminum_manual'.$i);
			$data['datas'][$i]['et_warnalabel'] = $this->input->post('et_warnalabel'.$i);
			//kondisi jika data kosong
			if($data['datas'][$i]['et_satuan'] == '' && $data['datas'][$i]['et_satuan_manual'] == '')
				$data['datas'][$i]['et_satuan'] = '-';
			if($data['datas'][$i]['et_takaran'] == '' && $data['datas'][$i]['et_takaran_manual'] == '')
				$data['datas'][$i]['et_takaran'] = '-';
			if($data['datas'][$i]['et_waktuminum'] == '' && $data['datas'][$i]['et_waktuminum_manual'] == '')
				$data['datas'][$i]['et_waktuminum'] = '-';

			if($data['datas'][$i]['et_warnalabel'] == 'Putih') {
				array_push($data_putih, $data['datas'][$i]);
				$jumlaharrputih++;
			}
		}
		$data['datas'] = $data_putih;
		$data['jumlaharr'] = $jumlaharrputih;
		
		//Send API POST
		$data_api['idksolabel'] = '15';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API, $data_api, array(CURLOPT_BUFFERSIZE => 100));

		$this->load->view('cetak_etiket', $data);
	}

	public function cetak_etiketbiru(){
		$data_biru = [];
		$jumlaharrbiru = 0;
		$data['et_nama'] = $this->input->post('et_nama');
		$data['et_rm'] = $this->input->post('et_rm');
		$data['et_title'] = $this->input->post('et_title');
		$data['et_ttl'] = $this->input->post('et_ttl');
		$data['jumlaharr'] = $this->input->post('jumlaharr');
		for($i=0; $i<$data['jumlaharr']; $i++) {
			$data['datas'][$i]['et_namaobat'] = $this->input->post('et_namaobat'.$i);
			$data['datas'][$i]['et_jumlahobat'] = $this->input->post('et_jumlahobat'.$i);
			$data['datas'][$i]['et_aturan'] = $this->input->post('et_aturan'.$i);
			$data['datas'][$i]['et_satuan'] = $this->input->post('et_satuan'.$i);
			$data['datas'][$i]['et_satuan_manual'] = $this->input->post('et_satuan_manual'.$i);
			$data['datas'][$i]['et_takaran'] = $this->input->post('et_takaran'.$i);
			$data['datas'][$i]['et_takaran_manual'] = $this->input->post('et_takaran_manual'.$i);
			$data['datas'][$i]['et_waktuminum'] = $this->input->post('et_waktuminum'.$i);
			$data['datas'][$i]['et_waktuminum_manual'] = $this->input->post('et_waktuminum_manual'.$i);
			$data['datas'][$i]['et_warnalabel'] = $this->input->post('et_warnalabel'.$i);
			//kondisi jika data kosong
			if($data['datas'][$i]['et_satuan'] == '' && $data['datas'][$i]['et_satuan_manual'] == '')
				$data['datas'][$i]['et_satuan'] = '-';
			if($data['datas'][$i]['et_takaran'] == '' && $data['datas'][$i]['et_takaran_manual'] == '')
				$data['datas'][$i]['et_takaran'] = '-';
			if($data['datas'][$i]['et_waktuminum'] == '' && $data['datas'][$i]['et_waktuminum_manual'] == '')
				$data['datas'][$i]['et_waktuminum'] = '-';

			if($data['datas'][$i]['et_warnalabel'] == 'Biru') {
				array_push($data_biru, $data['datas'][$i]);
				$jumlaharrbiru++;
			}
		}
		$data['datas'] = $data_biru;
		$data['jumlaharr'] = $jumlaharrbiru;
		
		//Send API POST
		$data_api['idksolabel'] = '16';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API, $data_api, array(CURLOPT_BUFFERSIZE => 100));

		$this->load->view('cetak_etiket', $data);
	}


	public function getPatient($norm){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://192.168.0.10/simrs/api/pasien/no_rm/".$norm,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Basic YWRtaW46UmFoYXNpYWJyMA==",
		    "Postman-Token: 05e949cd-acaa-40d8-a039-d548867906ec",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$result = json_decode($response);
		return $result;
	}

	public function getRecipe($recipe){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://192.168.0.10/simrs/api/resep/no_resep/".$recipe,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Basic YWRtaW46UmFoYXNpYWJyMA==",
		    "Postman-Token: 7f62f2ef-4f15-456b-b5f6-2c8a9f5f07a2",
		    "cache-control: no-cache"
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$result_recipe = json_decode($response);
		return $result_recipe;
	}


	public function generate_barcode($sparepart_code, $scale=6, $fontsize=18, $thickness=30,$dpi=72) {
	// CREATE BARCODE GENERATOR
	// Including all required classes
	require_once( APPPATH . 'libraries/barcodegen/BCGFontFile.php');
	require_once( APPPATH . 'libraries/barcodegen/BCGColor.php');
	require_once( APPPATH . 'libraries/barcodegen/BCGDrawing.php');

	// Including the barcode technology
	// Ini bisa diganti-ganti mau yang 39, ato 128, dll, liat di folder barcodegen
	require_once( APPPATH . 'libraries/barcodegen/BCGcode39.barcode.php');

	// Loading Font
	// kalo mau ganti font, jangan lupa tambahin dulu ke folder font, baru loadnya di sini
	//$font = new BCGFontFile(APPPATH . 'libraries/font/Arial.ttf', $fontsize);

	// Text apa yang mau dijadiin barcode, biasanya kode produk
	$text = $sparepart_code;

	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255);

	$drawException = null;
	try {
	$code = new BCGcode39(); // kalo pake yg code39, klo yg lain mesti disesuaikan
	$code->setScale($scale); // Resolution
	$code->setThickness($thickness); // Thickness
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	//$code->setFont($font); // Font (or 0)
	$code->parse($text); // Text
	} catch(Exception $exception) {
	$drawException = $exception;
	}

	/* Here is the list of the arguments
	1 - Filename (empty : display on screen)
	2 - Background color */
	$drawing = new BCGDrawing('', $color_white);
	if($drawException) {
	$drawing->drawException($drawException);
	} else {
	$drawing->setDPI($dpi);
	$drawing->setBarcode($code);
	$drawing->draw();
	}
	// ini cuma labeling dari sisi aplikasi saya, penamaan file menjadi png barcode.
	$filename_img_barcode = $sparepart_code.'.png';
	// folder untuk menyimpan barcode
	$drawing->setFilename( FCPATH . 'barcode/'. $filename_img_barcode);
	// proses penyimpanan barcode hasil generate
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

	return $filename_img_barcode;
	}

}	

