<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Yudidi</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>

   <?php
	$kode = str_replace(' ', '-', $yd_rm); //untuk generate barcode
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>
	
</head>


<body>
	<div style="font-size: 13px; font-family: Arial, Helvetica, sans-serif;">
	<div class="panel" style="top: 1px; left: 6px; position: absolute;">
		<img src="<?=base_url();?>assets/img/logo-basoeni.png" width="35px" height="43px">
	</div>
	<table style="width: 100%;margin-top: 2px" border="0px">
		<tr>
			<td colspan="3"><center><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instalasi Farmasi</b></center></td>
		</tr>
		<tr>
			<td colspan="3" style="font-size:10px;"><center><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RSUD. R.A BASOENI</b></center></td>
		</tr>
		<tr>
			<td colspan="3" style="font-size:8px;"><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jalan Raya Gedeg No. 17 Telp. (0321) 364752</center></td>
		</tr>
		<tr>
			<td colspan="3" style="border-top:1pt solid black;"></td>
		<tr>
		<tr>
			<td style="width:68px"><b>&nbsp;&nbsp;Nama</b></td>
			<td><b>:&nbsp;</b></td>
			<td><b><?=$yd_title;?>, <?=SUBSTR($yd_nama, 0, 30);?></b></td>
			<!-- <td><b>Tn, Muhammad Jabir Al Haiyan</b></td> -->
		</tr>
		</tr>
			<td><b>&nbsp;&nbsp;No RM</b></td>
			<td><b>:&nbsp;</b></td>
			<td><b><?=$yd_rm;?></b></td>
			<!-- <td><b>123456</b></td> -->
		</tr>
		<tr>
			<td><b>&nbsp;&nbsp;Tgl Lahir</b></td>
			<td><b>:</b></td>
			<td><b><?=tanggal_format($yd_ttl);?></b></td>
			<!-- <td><b>12-12-1212</b></td> -->
		</tr>
		<tr>
			<td colspan="3" style="padding-top:5px"></td>
		<tr>
		<tr>
			<td colspan="3" style="padding-top:5px;border-top:1pt dashed black;"></td>
		<tr>
		<tr>
			<td colspan="3"><b>&nbsp;&nbsp;Tanggal Pemberian : <?=tanggal_format($yd_tglpemberian);?></b></td>
			<!-- <td colspan="3"><b>&nbsp;&nbsp;Tanggal Pemberian : 10-10-2012</b></td> -->
		</tr>
		<tr>
			<td colspan="3"><b>&nbsp;&nbsp;Waktu Pemberian &nbsp;&nbsp;&nbsp;: <?=$yd_waktu;?> <?=$yd_waktu_manual;?></b></td>
			<!-- <td colspan="3"><b>&nbsp;&nbsp;Waktu Pemberian &nbsp;&nbsp;&nbsp;: 06.00</b></td> -->
		</tr>
		<tr>
			<td colspan="3"><center><b><?=$yd_aturan;?> <?=$yd_aturan_manual;?></b></center></td>
			<!-- <td colspan="3"><center><b>Sebelum Makan</b></center></td> -->
		</tr>
	</table>
</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>
<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script> -->


<!-- Langsung Cetak -- Cuman Butuh Waktu untuk generate-->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
