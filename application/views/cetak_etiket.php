<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Etiket</title>
	
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>	

<?php
	$tanggal = date('d-m-Y');
?>
</head>
<body>
	<div style="font-size: 10px; font-family: Arial, Helvetica, sans-serif">
	
	<?php for($i=0; $i<$jumlaharr; $i++){ ;?>
	<table style="width: 100%" border="0">
		<tr>
			<td rowspan="4" width="40px">
				<center><img src="<?=base_url();?>assets/img/logo-basoeni.png" width="35px" height="43px"></center>
			</td>
		</tr>
		<tr>
			<td colspan="5"><center><b>INSTALASI FARMASI</b></center></td>
		</tr>
		<tr>
			<td colspan="5"><center><b>RSUD. R.A BASOENI</b></center></td>
		</tr>
		<tr>
			<td colspan="5" style="font-size:8px;"><center>Jalan Raya Gedeg No. 17 Telp. (0321) 364752</center></td>
		</tr>
		<tr style="border-bottom:1pt solid black;">
			<td colspan="6"></td>
		</tr>
		<tr>
			<td style="width:50px"><b>&nbsp;Tanggal</b></td>
			<td><b>:</b></td>
			<td style="width:80px"><b><?=$tanggal;?></b></td>
			<td style="width:40px"><b>No RM</b></td>
			<td><b>:</b></td>
			<td><b><?=$et_rm;?></b></td>
		</tr>
		<tr>
			<td><b>&nbsp;Nama</b></td>
			<td><b>:</b></td>
			<td colspan="4"><b><?=SUBSTR($et_nama, 0, 22);?>, <?=$et_title;?></b></td>
		</tr>
		<tr>
			<td><b>&nbsp;Tgl Lahir</b></td>
			<td><b>:</b></td>
			<td colspan="4"><b><?=$et_ttl;?></b></td>
		</tr>
		<?php
			$split = explode('x', $datas[$i]['et_aturan']);
			$et_aturan_depan = $split[0];
			$et_aturan_belakang = $split[1];
		?>
		<tr>
			<td colspan="6" style="font-size:12px;">
				<!-- <center><b><?=$et_aturan_depan;?> Sehari <?=$et_aturan_belakang;?></b></center> -->
				<center><b><?=$datas[$i]['et_aturan'];?></b></center>
			</td>
		</tr>
		<tr>
			<td colspan="6" style="font-size:12px;">
				<center><b><?=$datas[$i]['et_satuan'];?><?=$datas[$i]['et_satuan_manual'];?></b></center>
			</td>
		</tr>
		<tr>
			<td colspan="6" style="font-size:12px;">
				<center><b><?=$datas[$i]['et_takaran'];?><?=$datas[$i]['et_takaran_manual'];?></b></center>
			</td>
		</tr>
		<tr>
			<td colspan="6" style="font-size:12px;border-bottom:1pt solid black;">
				<center><b><?=$datas[$i]['et_waktuminum'];?><?=$datas[$i]['et_waktuminum_manual'];?></b></center>
			</td>
		</tr>
		<tr>
			<td colspan="6"><center><b>Nama Obat : <?=SUBSTR($datas[$i]['et_namaobat'], 0, 65);?> (<?=$datas[$i]['et_jumlahobat'];?>)</b></center></td>
		</tr>
	</table>
	<?php 
		if(strlen($datas[$i]['et_namaobat']) <= 28) echo '<br>';
	?>

	<?php } ?>

</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>

<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script> -->

<!-- Langsung Cetak -->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
