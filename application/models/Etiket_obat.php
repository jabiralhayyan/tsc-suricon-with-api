<?php
class Etiket_obat extends CI_Model {
 private $table = 'f_resep';
 public function get($select, $where=NULL){
 		if (!empty($where)) {
			$this->db->where($where);
		}   
        $this->db->select($select);
        $this->db->from($this->table);
        $this->db->join('f_resep_detail', 'f_resep_detail.resep_id = f_resep.id_resep');
        $this->db->join('f_obat', 'f_obat.id_obat = f_resep_detail.obat_id');
        $this->db->join('t_pasien', 't_pasien.no_rm = f_resep.no_rm');
		$query = $this->db->get();
		return $query;
    }
 }