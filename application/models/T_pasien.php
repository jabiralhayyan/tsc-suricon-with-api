<?php

 class T_pasien extends CI_Model {

 private $table = 't_pasien';

 public function get($select, $where=NULL){
 		if (!empty($where)) {
			$this->db->where($where);
		}   
    	$this->db->select($select);
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query;
    }

 public function insert($data){
        $this->db->insert($this->table, $data);
    }
  
 public function update($set, $where){
        $this->db->where($where);
        $this->db->update($this->table, $set);
    }
  
 public function delete($where){
        $this->db->where($where);
        $this->db->delete($this->table);
    }
 }