<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->API = 'http://masterdata.unotech.id/api/api_bartenderlabel/';
		$this->API_KARTU = 'http://masterdata.unotech.id/api/api_bartenderkartu/';
	}

	public function index()
	{
		$data['title'] = 'Rawat Inap';
		$data['query'] = '';
		$caripasien = $this->input->post('caripasien');
		if ($caripasien != '') {
			  //$json_pasien = '{"status":true, "data":{"no_rm":"5736312","title":"Tn","nama":"MUHAMMAD JABIR AL HAIYAN","nik":"3578042308940003","tanggal_lahir":"1991-07-16","alamat":"Selorejo"}}';
			  //$result = json_decode($json_pasien);
			$result = $this->getPatient($caripasien);
			if(isset($result->status) && $result->status == "true") {
				$data['query'] = 'success';
				$data['no_rm'] = $result->data->no_rm;
				$data['nama'] = $result->data->nama;
				$data['nik'] = $result->data->nik;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
				$data['alamat'] = $result->data->alamat;
				$this->generate_barcode($data['no_rm']);
			}
		}
		//Nav
		$data['nav_rm_active'] = 'active';
		$data['nav_et_active'] = '';
		$data['nav_ga_active'] = '';
		$data['nav_gd_active'] = '';
		$data['nav_kp_active'] = '';
		$data['nav_ll_active'] = '';
		$data['nav_li_active'] = '';
		$data['nav_lg_active'] = '';
		//Tab
		$data['tab_rm_active'] = 'in active';
		$data['tab_et_active'] = '';
		$data['tab_ga_active'] = '';
		$data['tab_gd_active'] = '';
		$data['tab_kp_active'] = '';
		$data['tab_ll_active'] = '';
		$data['tab_li_active'] = '';
		$data['tab_lg_active'] = '';
		$this->load->view('home',$data);
	}

	public function label_igd() {
		$data['title'] = 'Instalasi Gawat Darurat';
		$data['query'] = '';
		$caripasien = $this->input->post('caripasien');
		if ($caripasien != '') {
			 //$json_pasien = '{"status":true, "data":{"no_rm":"5736312","title":"Tn","nama":"MUHAMMAD JABIR AL HAIYAN","nik":"3578042308940003","tanggal_lahir":"1991-07-16","alamat":"Selorejo"}}';
			  //$result = json_decode($json_pasien);
			$result = $this->getPatient($caripasien);
			if(isset($result->status) && $result->status == "true") {
				$data['query'] = 'success';
				$data['no_rm'] = $result->data->no_rm;
				$data['nama'] = $result->data->nama;
				$data['nik'] = $result->data->nik;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
				$data['alamat'] = $result->data->alamat;
				$this->generate_barcode($data['no_rm']);
			}
		}
		//Nav
		$data['nav_rm_active'] = '';
		$data['nav_et_active'] = '';
		$data['nav_ga_active'] = '';
		$data['nav_gd_active'] = '';
		$data['nav_kp_active'] = '';
		$data['nav_ll_active'] = '';
		$data['nav_li_active'] = 'active';
		$data['nav_lg_active'] = '';
		//Tab
		$data['tab_rm_active'] = '';
		$data['tab_et_active'] = '';
		$data['tab_ga_active'] = '';
		$data['tab_gd_active'] = '';
		$data['tab_kp_active'] = '';
		$data['tab_ll_active'] = '';
		$data['tab_li_active'] = 'in active';
		$data['tab_lg_active'] = '';
		$this->load->view('home',$data);
	}

	public function label_gizi() {
		$data['title'] = 'Label Gizi';
		$data['query'] = '';
		$caripasien = $this->input->post('caripasien');
		if ($caripasien != '') {
			// $json_pasien = '{"status":true, "data":{"no_rm":"5736312","title":"Tn","nama":"MUHAMMAD JABIRA","tanggal_lahir":"1991-07-16","alamat":"Selorejo"}}';
			// $result = json_decode($json_pasien);

			$result = $this->getPatient($caripasien);
			if(isset($result->status) && $result->status == "true") {
				$data['query'] = 'success';
				$data['no_rm'] = $result->data->no_rm;
				$data['nama'] = $result->data->nama;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
				$data['alamat'] = $result->data->alamat;
				$this->generate_barcode($data['no_rm']);
			}
		}
		//Nav
		$data['nav_rm_active'] = '';
		$data['nav_et_active'] = '';
		$data['nav_ga_active'] = '';
		$data['nav_gd_active'] = '';
		$data['nav_kp_active'] = '';
		$data['nav_ll_active'] = '';
		$data['nav_li_active'] = '';
		$data['nav_lg_active'] = 'active';
		//Tab
		$data['tab_rm_active'] = '';
		$data['tab_et_active'] = '';
		$data['tab_ga_active'] = '';
		$data['tab_gd_active'] = '';
		$data['tab_kp_active'] = '';
		$data['tab_ll_active'] = '';
		$data['tab_li_active'] = '';
		$data['tab_lg_active'] = 'in active';
		$this->load->view('home',$data);
	}

	public function kartu_pasien() {
	 	$data['title'] = 'Kartu Pasien';
	 	$data['query'] = '';
	 	$caripasien = $this->input->post('caripasien');
	 	if ($caripasien != '') {
	 		$result = $this->getPatient($caripasien);
			// $json_pasien = '{"status":true, "data":{"no_rm":"5736312","title":"Tn","nama":"MUHAMMAD JABIR AL H","tanggal_lahir":"1991-07-16","alamat":"Selorejo"}}';
			// $result = json_decode($json_pasien);
			if(isset($result->status) && $result->status == "true") {
				$data['query'] = 'success';
				$data['no_rm'] = $result->data->no_rm;
				$data['nama'] = $result->data->nama;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
				$data['alamat'] = $result->data->alamat;
				$this->generate_barcode($data['no_rm']);
			}
	 	}
	 	//Nav
	 	$data['nav_rm_active'] = '';
	 	$data['nav_et_active'] = '';
	 	$data['nav_ga_active'] = '';
	 	$data['nav_gd_active'] = '';
	 	$data['nav_kp_active'] = 'active';
	 	$data['nav_ll_active'] = '';
	 	$data['nav_li_active'] = '';
		$data['nav_lg_active'] = '';
	 	//Tab
	 	$data['tab_rm_active'] = '';
	 	$data['tab_et_active'] = '';
	 	$data['tab_ga_active'] = '';
	 	$data['tab_gd_active'] = '';
	 	$data['tab_kp_active'] = 'in active';
	 	$data['tab_ll_active'] = '';
	 	$data['tab_li_active'] = '';
		$data['tab_lg_active'] = '';
	 	$this->load->view('home',$data);
	 }

	//Bagian Fungsi Cetak
	public function cetak_rawatinap(){
		$data['ri_rm'] = $this->input->post('ri_rm');
		$data['ri_nama'] = $this->input->post('ri_nama');
		$data['ri_nik'] = $this->input->post('ri_nik');
		$data['ri_title'] = $this->input->post('ri_title');
		$data['ri_ttl'] = $this->input->post('ri_ttl');
		//Send API POST
		$data_api['idksolabel'] = '10';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API, $data_api, array(CURLOPT_BUFFERSIZE => 100));
		$this->load->view('cetak_rawatinap', $data);
	}

	public function cetak_labeligd(){
		$data['li_rm'] = $this->input->post('li_rm');
		$data['li_nama'] = $this->input->post('li_nama');
		$data['li_nik'] = $this->input->post('li_nik');
		$data['li_title'] = $this->input->post('li_title');
		$data['li_ttl'] = $this->input->post('li_ttl');
		//Send API POST
		$data_api['idksolabel'] = '11';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API, $data_api, array(CURLOPT_BUFFERSIZE => 100));
		$this->load->view('cetak_labeligd', $data);
	}

	public function cetak_labelgizi(){
		$data['lg_tanggaldiet'] = $this->input->post('lg_tanggaldiet');
		$data['lg_nama'] = $this->input->post('lg_nama');
		$data['lg_rm'] = $this->input->post('lg_rm');
		$data['lg_ttl'] = $this->input->post('lg_ttl');
		$data['lg_nobed'] = $this->input->post('lg_nobed');
		$data['lg_jenisdiet'] = $this->input->post('lg_jenisdiet');
		$data['lg_waktudiet'] = $this->input->post('lg_waktudiet');
		$data['lg_keterangan'] = $this->input->post('lg_keterangan');
		//Send API POST
		$data_api['idksolabel'] = '12';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API, $data_api, array(CURLOPT_BUFFERSIZE => 100));
		$this->load->view('cetak_labelgizi', $data);
	}

	public function cetak_kartupasien(){
		$data['kp_nama'] = $this->input->post('kp_nama');
		$data['kp_title'] = $this->input->post('kp_title');
		$data['kp_rm'] = $this->input->post('kp_rm');
		$data['kp_alamat'] = $this->input->post('kp_alamat');
		$data['kp_ttl'] = $this->input->post('kp_ttl');
		//Send API POST
		$data_api['idksokartu'] = '2';
		$data_api['created_at'] = date('Y-m-d');
		$this->curl->simple_post($this->API_KARTU, $data_api, array(CURLOPT_BUFFERSIZE => 100));
		$this->load->view('cetak_kartupasien', $data);
	}

	public function cetak_kartupasien_test(){
		$this->load->view('cetak_kartupasien_test');
	}

	public function getPatient($norm){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://192.168.0.10/simrs/api/pasien/no_rm/".$norm,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Basic YWRtaW46UmFoYXNpYWJyMA==",
		    "Postman-Token: 05e949cd-acaa-40d8-a039-d548867906ec",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$result = json_decode($response);
		return $result;
	}
	
	public function generate_barcode($sparepart_code, $scale=6, $fontsize=18, $thickness=30,$dpi=72) {
	// CREATE BARCODE GENERATOR
	// Including all required classes
	require_once( APPPATH . 'libraries/barcodegen/BCGFontFile.php');
	require_once( APPPATH . 'libraries/barcodegen/BCGColor.php');
	require_once( APPPATH . 'libraries/barcodegen/BCGDrawing.php');

	// Including the barcode technology
	// Ini bisa diganti-ganti mau yang 39, ato 128, dll, liat di folder barcodegen
	require_once( APPPATH . 'libraries/barcodegen/BCGcode39.barcode.php');

	// Loading Font
	// kalo mau ganti font, jangan lupa tambahin dulu ke folder font, baru loadnya di sini
	//$font = new BCGFontFile(APPPATH . 'libraries/font/Arial.ttf', $fontsize);

	// Text apa yang mau dijadiin barcode, biasanya kode produk
	$text = $sparepart_code;

	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255);

	$drawException = null;
	try {
	$code = new BCGcode39(); // kalo pake yg code39, klo yg lain mesti disesuaikan
	$code->setScale($scale); // Resolution
	$code->setThickness($thickness); // Thickness
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	//$code->setFont($font); // Font (or 0)
	$code->parse($text); // Text
	} catch(Exception $exception) {
	$drawException = $exception;
	}

	/* Here is the list of the arguments
	1 - Filename (empty : display on screen)
	2 - Background color */
	$drawing = new BCGDrawing('', $color_white);
	if($drawException) {
	$drawing->drawException($drawException);
	} else {
	$drawing->setDPI($dpi);
	$drawing->setBarcode($code);
	$drawing->draw();
	}
	// ini cuma labeling dari sisi aplikasi saya, penamaan file menjadi png barcode.
	$filename_img_barcode = $sparepart_code.'.png';
	// folder untuk menyimpan barcode
	$drawing->setFilename( FCPATH . 'barcode/'. $filename_img_barcode);
	// proses penyimpanan barcode hasil generate
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

	return $filename_img_barcode;
	}
	
	public function generate_qrcode($nomor_qrcode){
       $this->load->library('ciqrcode'); //pemanggilan library QR CODE
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 
        $image_name=$nomor_qrcode.'.png'; //buat name dari qr code sesuai dengan nim
        $params['data'] = $nomor_qrcode; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/qrcode/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
    }

}	

