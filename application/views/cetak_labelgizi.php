<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Label Gizi</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>

   <?php
	$kode = str_replace(' ', '-', $lg_rm); //untuk generate barcode
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>
	
</head>
<body>
	<div style="font-size: 14px; font-family: Arial, Helvetica, sans-serif;">
	<table style="width: 100%; margin-top: 5px">
		<tr>
			<td width="80px"><b>&nbsp;Tgl Diet</b></td>
			<td><b>:</b></td>
			<td><?=$lg_tanggaldiet;?></td>
			<td><b>No.</b> <?=$lg_nobed;?></td>
		</tr>
		<tr>
			<td><b>&nbsp;Waktu Diet</b></td>
			<td><b>:</b></td>
			<td colspan="2"><?=$lg_waktudiet;?></td>
		</tr>
		<tr>
			<td><b>&nbsp;No. RM</b></td>
			<td><b>:</b></td>
			<td colspan="2"><?=$lg_rm;?></td>
		</tr>
		<tr>
			<td><b>&nbsp;Nama</b></td>
			<td><b>:</b></td>
			<td colspan="2"><?=SUBSTR($lg_nama, 0, 14);?></td>
		</tr>
		<tr>
			<td><b>&nbsp;Tgl. Lahir</b></td>
			<td><b>:</b></td>
			<td colspan="2"><?=$lg_ttl;?></td>
		</tr>
		<tr>
			<td><b>&nbsp;Jenis Diet</b></td>
			<td><b>:</b></td>
			<td colspan="2"><?=$lg_jenisdiet;?></td>
		</tr>
		<tr>
			<td><b>&nbsp;Keterangan</b></td>
			<td><b>:</b></td>
			<td colspan="2"><?=SUBSTR($lg_keterangan, 0, 20);?></td>
		</tr>
		<tr>
			<td colspan="4"><b>&nbsp;</b></td>
		</tr>
		<tr>
			<td colspan="4" style="text-align: right;"><b>SEGERA DIMAKAN&nbsp;</b></td>
		</tr>
	</table>
</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>
<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script> -->

<!-- Langsung Cetak -- Cuman Butuh Waktu untuk generate-->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
