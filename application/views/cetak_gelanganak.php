<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Gelang Anak</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>

   <?php
    $kode = str_replace(' ', '-', $ga_rm);
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>
</head>
<body>
	<div style="font-size: 10px; font-family: Arial, Helvetica, sans-serif;">
	<table style="width: 100%; margin-top: 5px">
		<tr>
			<td rowspan="4" width="60px">
				&nbsp;<img src="<?=base_url();?>assets/img/logo-basoeni-new.png" >
			</td>
		</tr>
		<tr>
			<td><b><?=$ga_rm;?></b></td>
		</tr>
		<tr><td><b><?=SUBSTR($ga_nama, 0, 30);?>, <?=$ga_title;?></b></td>
		</tr>
		<tr><td><b><?=tanggal_format($ga_ttl);?></b></td>
		</tr>
	</table>
</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>
<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script>
 -->

<!-- Langsung Cetak -- Cuman Butuh Waktu untuk generate-->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
