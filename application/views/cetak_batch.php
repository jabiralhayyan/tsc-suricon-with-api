<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Barcode</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>

   <?php
	$kode = str_replace(' ', '-', $bt_nobatch); //untuk generate barcode
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>
<style type="text/css">
	.rotate270 {
    -webkit-transform: rotate(270deg);
    -moz-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
    transform: rotate(270deg);
}
</style>
</head>
					
<body>
	<div style="font-size: 11px; font-family: Arial, Helvetica, sans-serif;">
	<div style="top: 34px; right: 1px; position: absolute;">
		<img class="rotate270" alt="" src="<?php echo base_url();?>barcode/<?php echo $kode;?>.png" width="50px" height="25px">
		<!-- <img class="rotate270" alt="" src="<?php echo base_url();?>barcode/003833.png" width="50px" height="25px"> -->
	</div>
	<table style="width: 100%;margin-left: 5px" border="0px">
		<tr>
			<td colspan="4"><b><?=SUBSTR($bt_namaobat, 0, 32);?></b></td>
			<!-- <td colspan="3"><b>Paracetamol 300 Mg Nama panjang banget nama obatnyaggggg</b></td> -->
		</tr>
		<tr>
			<td width="76px"><b>No. Batch</b></td>
			<td><b>:</b></td>
			<td><b><?=$bt_nobatch;?></b></td>
			<!-- <td><b>125</b></td> -->
		</tr>
		<tr>
			<td><b>No. Registrasi</b></td>
			<td><b>:</b></td>
			<td><b><?=$bt_noregistrasi;?></b></td>
		</tr>
		<tr>
			<td><b>MFG</b></td>
			<td><b>:</b></td>
			<td><b><?=tanggal_format($bt_tglproduksi);?></b></td>
			<!-- <td><b>12-12-1212</b></td> -->
		</tr>
		<tr>
			<td><b>EXP</b></td>
			<td><b>:</b></td>
			<td><b><?=tanggal_format($bt_tglexpired);?></b></td>
			<!-- <td><b>12-12-1212</b></td> -->
		</tr>
	</table>
</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>
<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script> -->


<!-- Langsung Cetak -- Cuman Butuh Waktu untuk generate-->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
