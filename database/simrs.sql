/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : simrs

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-14 20:14:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pasien
-- ----------------------------
DROP TABLE IF EXISTS `pasien`;
CREATE TABLE `pasien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_pasien` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rkm_medis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pasien
-- ----------------------------
INSERT INTO `pasien` VALUES ('1', 'Jabir Al Hayyan', '11111', 'L', 'Jl. Smea 27, Wonokromo, Surabaya', '1994-11-25', '2018-11-25 23:52:22', '2019-01-14 20:12:59');
INSERT INTO `pasien` VALUES ('2', 'Imam Muttaqien', '22222', 'L', 'Jl. Siwalankerto Utara, Surabaya', '1990-11-01', '2018-11-26 00:07:20', '2019-01-14 20:13:29');
INSERT INTO `pasien` VALUES ('3', 'Cois De limit', '33333', 'L', 'Gedangan Kulon 100 KM, Sidoarjo', '1980-10-10', '2019-01-14 20:13:39', '2019-01-14 20:13:42');
