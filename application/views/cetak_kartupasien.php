<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Kartu Pasien</title>
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"> -->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<style type="text/css">

/*body{
    background: url("<?php echo base_url();?>assets/img/kartu2.png");
	background-repeat: no-repeat;
	-webkit-print-color-adjust: exact;
	background-size: 85.3mm 53.7mm;
}*/
/*@media print {
{
	-webkit-print-color-adjust: exact;
	background: #222;
	color: #eee;
}*/

.rotate270 {
    -webkit-transform: rotate(270deg);
    -moz-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
    transform: rotate(270deg);

</style>

   <?php
    $kode = str_replace(' ', '-', $kp_rm);
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>
</head>
<body>
	
<!-- 	<div class="panel" style="top: 53px; left: 68px; position: absolute; width:250px; height: 68px;">
		<div class="panel-body">
			<strong>
				<p style="font-size: 14px;text-align:right;"><b><strong><?=SUBSTR($kp_nama, 0, 19);?>, <?=$kp_title;?>&nbsp;</strong></b></p> 
				<p style="font-size: 14px;margin-top: -11px;text-align:right;"><b><strong><?=$kp_rm;?>&nbsp;</strong></b></p> 
				<p style="font-size: 14px;margin-top: -14px;text-align:right;margin-bottom: 3px"><b><strong><?=tanggal_format($kp_ttl);?>&nbsp;</strong></b></p> 
			</strong>
			
		</div>
		<img src="<?php echo base_url();?>barcode/<?php echo $kode;?>.png" style="width: 93px;height: 22px; margin-top:0px;margin-left:153px">
	</div> -->

	<div class="panel" style="top: 1.37cm;left: 2.7cm;position: absolute;width:56mm;height:3.9mm">
		<div class="panel-body">
			<p style="font-size: 14px;text-align: right"><b><strong><?=SUBSTR($kp_nama, 0, 19);?>, <?=$kp_title;?>&nbsp;</strong></b></p>
		</div>
	</div>
	<div class="panel" style="top: 1.86cm;left: 6.05cm;position: absolute;width:22.5mm;height:3.9mm">
		<div class="panel-body">
			<p style="font-size: 14px;text-align: right"><b><strong><?=$kp_rm;?>&nbsp;</strong></b></p>
		</div>
	</div>
	<div class="panel" style="top: 2.3cm;left: 6.05cm;position: absolute;width:22.5mm;height:3.9mm">
		<div class="panel-body">
			<p style="font-size: 14px;text-align: right"><b><strong><?=tanggal_format($kp_ttl);?>&nbsp;</strong></b></p>
		</div>
	</div>
	<div class="panel" style="top: 3.14cm;left: 5.8cm;position: absolute;">
		<div class="panel-body">
			<img src="<?php echo base_url();?>barcode/<?php echo $kode;?>.png" style="width:24.5mm;height:6.3mm">
		</div>
	</div>

</body>
</html>
<?php require_once('template/footer.php'); ?>
<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
 <script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script> 
 -->

<!-- Langsung Cetak -- Cuman Butuh Waktu untuk generate-->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		//window.close();
	});
</script>
