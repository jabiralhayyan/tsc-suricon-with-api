<?php require_once('template/header.php'); ?>

<?php
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	function hitung_umur($tanggal, $differenceFormat = '%y')
	{
    	$datetime1 = date_create(date('Y-m-d'));
    	$datetime2 = date_create($tanggal);
    	$interval = date_diff($datetime1, $datetime2);
    	return $interval->format($differenceFormat);
	}
?>

<style>
.note-toolbar{
	display: none;
}
</style>

<div class="container-fluid mimin-wrapper">
	<?php require_once('template/nav.php'); ?>
	<!-- start: content -->
	<div id="content">
		<div class="tabs-wrapper text-center">             
			<div class="panel box-shadow-none text-left content-header">
				<div class="panel-body" style="padding-bottom:0px;">
					<div class="col-md-12">
						<h3 class="animated fadeInLeft">Home</h3>
						<p class="animated fadeInDown">
							Home <span class="fa-angle-right fa"></span> Cetak
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="col-md-12 tabs-area">
								<div class="liner"></div>
								<ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo5">
									<!-- RM -->
									<li class="<?php echo $nav_rm_active ;?>">
										<a href="#cetak-rm" data-toggle="tab" title="Cetak Rekam Medis">
											<span class="round-tabs four">
												<i class="fa fa-medkit"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Rawat Inap</b></p>
									</li>

									<!-- Label IGD -->
									<li class="<?php echo $nav_li_active ;?>">
										<a href="#label-igd" data-toggle="tab" title="Cetak Label IGD">
											<span class="round-tabs four">
												<i class="fa fa-ambulance"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Label IGD</b></p>
									</li>

									<!-- Label GIZI -->
									<li class="<?php echo $nav_lg_active ;?>">
										<a href="#label-gizi" data-toggle="tab" title="Cetak Label Gizi">
											<span class="round-tabs four">
												<i class="fa fa-cutlery"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Label Gizi</b></p>
									</li>
									
									<!-- Kartu Pasien -->
									<li class="<?php echo $nav_kp_active ;?>">
										<a href="#cetak-kartu-pasien" data-toggle="tab" title="Kartu Pasien">
											<span class="round-tabs three">
												<i class="fa fa-credit-card"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Kartu Pasien</b></p>
									</li>

								</ul>

								<div class="tab-content tab-content-v5">
									<!-- FORM REKAM MEDIS -->
									<div class="tab-pane fade <?php echo $tab_rm_active ;?>" id="cetak-rm">
										<div class="col-md-12 panel-body">
											<form class="form-horizontal" action="<?php echo site_url(); ?>home" method="POST">
												<h4><b>Rawat Inap</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Pasien</label>
														<div class="col-sm-8">
															<input name="caripasien" type="text" class="form-control border-bottom" placeholder="Masukkan No Rekam Medis ...." required>
														</div>
														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>
											<table class="table table-striped table-bordered" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Nama Pasien</th>
														<th>NIK</th>
														<th>No Rekam Medis</th>
														<th>Alamat</th>
														<th>Tanggal Lahir</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php if($query != ''){ ?>
														<tr>
															<td><?=$nama;?>, <?=$title;?></td>
															<td><?=$nik;?></td>
															<td><?=$no_rm;?></td>
															<td><?=$alamat;?></td>
															<td><?=tanggal_format($tanggal_lahir);?></td>
															<td>
																<form target="_blank" action="<?php echo site_url();?>home/cetak_rawatinap/" method="POST">
																	<input type="hidden" name="ri_rm" value="<?=$no_rm;?>">
																	<input type="hidden" name="ri_nama" value="<?=$nama;?>">
																	<input type="hidden" name="ri_nik" value="<?=$nik;?>">
																	<input type="hidden" name="ri_title" value="<?=$title;?>">
																	<input type="hidden" name="ri_alamat" value="<?=$alamat;?>">
																	<input type="hidden" name="ri_ttl" value="<?=$tanggal_lahir;?>">
																	<button type="submit" class="btn btn-sm btn-primary"><span class="icons icon-printer"></span> Cetak</button>
																</form>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>

								

							<!-- LABEL IGD -->
							<div class="tab-pane fade <?php echo $tab_li_active ;?>" id="label-igd">
										<div class="col-md-12 panel-body">
											<form class="form-horizontal" action="<?php echo site_url(); ?>home/label_igd" method="POST">
												<h4><b>Label IGD</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Pasien</label>
														<div class="col-sm-8">
															<input name="caripasien" type="text" class="form-control border-bottom" placeholder="Masukkan No Rekam Medis ...." required>
														</div>

														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>
											<table class="table table-striped table-bordered" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Nama Pasien</th>
														<th>NIK</th>
														<th>No Rekam Medis</th>
														<th>Alamat</th>
														<th>Tanggal Lahir</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php if($query != ''){ ?> 
														<tr>
															<td><?=$nama;?>, <?=$title;?></td>
															<td><?=$nik;?></td>
															<td><?=$no_rm;?></td>
															<td><?=$alamat;?></td>
															<td><?=tanggal_format($tanggal_lahir);?></td>
															<td>
																<form target="_blank" action="<?php echo site_url();?>home/cetak_labeligd/" method="POST">
																	<input type="hidden" name="li_rm" value="<?=$no_rm;?>">
																	<input type="hidden" name="li_nama" value="<?=$nama;?>">
																	<input type="hidden" name="li_nik" value="<?=$nik;?>">
																	<input type="hidden" name="li_title" value="<?=$title;?>">
																	<input type="hidden" name="li_alamat" value="<?=$alamat;?>">
																	<input type="hidden" name="li_ttl" value="<?=$tanggal_lahir;?>">
																	<button type="submit" class="btn btn-sm btn-primary"><span class="icons icon-printer"></span> Cetak</button>
																</form>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
							<!-- /END LABEL IGD -->


							<!-- LABEL GIZI -->
									<div class="tab-pane fade <?php echo $tab_lg_active ;?>" id="label-gizi">
										<div class="col-md-12 panel-body">
											<form class="form-horizontal" action="<?php echo site_url(); ?>home/label_gizi" method="POST" required>
												<h4><b>Label Gizi</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Pasien</label>
														<div class="col-sm-8">
															<input name="caripasien" type="text" class="form-control border-bottom" placeholder="Masukkan No Rekam Medis ....">
														</div>
														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>
											<?php if($query != ''){  ?>
											<form action="<?=site_url();?>home/cetak_labelgizi" target="_blank" method="POST">
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Tanggal Diet</label>
                              							<div class="col-sm-10">
                              								<input name="lg_tanggaldiet" type="text" class="form-control" value="<?=date('d-m-Y');?>" readonly>
                              							</div>
                            						</div>
                            						<div class="form-group" style="padding-top: 30px"><label class="col-sm-2 control-label text-right">Nama Pasien</label>
                              							<div class="col-sm-10">
                              								<input name="lg_nama" type="text" class="form-control" value="<?=$nama;?>" readonly>
                              							</div>
                            						</div>
                            						<div class="form-group" style="padding-top: 30px"><label class="col-sm-2 control-label text-right">Rekam Medis</label>
                              							<div class="col-sm-10">
                              								<input name="lg_rm" type="text" class="form-control" value="<?=$no_rm;?>" readonly>
                              							</div>
                            						</div>
                            						<div class="form-group" style="padding-top: 30px"><label class="col-sm-2 control-label text-right">Tanggal Lahir</label>
                              							<div class="col-sm-10">
                              								<input name="lg_ttl" type="text" class="form-control" value="<?=tanggal_format($tanggal_lahir);?>" readonly>
                              							</div>
                            						</div>
                            						<div class="form-group" style="padding-top: 30px"><label class="col-sm-2 control-label text-right">No. Bed</label>
                              							<div class="col-sm-10">
                              								<input name="lg_nobed" type="text" class="form-control" value="">
                              							</div>
                            						</div>
                            						<div class="form-group" style="padding-top: 30px"><label class="col-sm-2 control-label text-right">Jenis Diet</label>
						                              <div class="col-sm-10">
						                                <div class="col-sm-12 padding-0">
						                                  <select class="form-control" name="lg_jenisdiet" id="lg_jenisdiet">
						                                    <option>N</option>
						                                    <option>L</option>
						                                    <option>H</option>
						                                    <option>C</option>
						                                    <option>Puasa</option>
						                                  </select>
						                                </div>
						                              </div>
						                          </div>
						                          <div class="form-group" style="padding-top: 30px"><label class="col-sm-2 control-label text-right">Waktu Diet</label>
						                              <div class="col-sm-10">
						                                <div class="col-sm-12 padding-0">
						                                  <select class="form-control" name="lg_waktudiet" id="lg_waktudiet">
						                                    <option>PI</option>
						                                    <option>SI</option>
						                                    <option>SO</option>
						                                  </select>
						                                </div>
						                              </div>
						                          </div>
						                          <div class="form-group" style="padding-top: 30px"><label class="col-sm-2 control-label text-right">Keterangan</label>
                              							<div class="col-sm-10">
                              								<textarea name="lg_keterangan" class="form-control"></textarea>
                              							</div>
                            						</div>
						                      </div>
												<div class="col-md-12">
													<div class="form-group" style="margin-top:20px !important;">
														<button type="submit" class="btn ripple btn-3d btn-success">
															<div>
																<span>PRINT</span>
																<span class="icons icon-printer"></span>
															</div>
														</button> 
													</div>
												</div>
											</form>
											<?php } ?>
										</div>
									</div>
								<!-- /END LABEL GIZI -->
								
								<!-- KARTU PASIEN -->
									<div class="tab-pane fade <?php echo $tab_kp_active ;?>" id="cetak-kartu-pasien">
										<div class="col-md-12 panel-body">
											<form class="form-horizontal" action="<?php echo site_url(); ?>home/kartu_pasien" method="POST" required>
												<h4><b>Kartu Pasien</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Pasien</label>
														<div class="col-sm-8">
															<input name="caripasien" type="text" class="form-control border-bottom" placeholder="Masukkan No Rekam Medis">
														</div>

														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>

											<table class="table table-striped table-bordered" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Nama Pasien</th>
														<th>No Rekam Medis</th>
														<th>Alamat</th>
														<th>Tanggal Lahir</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php if($query != ''){  ?> 
														<tr>
															<td><?=$nama;?>, <?=$title;?></td>
															<td><?=$no_rm;?></td>
															<td><?=$alamat;?></td>
															<td><?=tanggal_format($tanggal_lahir);?></td>
															<td>
																<form target="_blank" action="<?php echo site_url();?>home/cetak_kartupasien/" method="POST">
																	<input type="hidden" name="kp_rm" value="<?=$no_rm;?>">
																	<input type="hidden" name="kp_nama" value="<?=$nama;?>">
																	<input type="hidden" name="kp_title" value="<?=$title;?>">
																	<input type="hidden" name="kp_alamat" value="<?=$alamat;?>">
																	<input type="hidden" name="kp_ttl" value="<?=$tanggal_lahir;?>">
																	<button type="submit" class="btn btn-sm btn-primary"><span class="icons icon-printer"></span> Cetak</button>
																</form>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
									<!-- /END KARTU PASIEN -->

								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>      
				</div>
			</div>
		</div>

		
		<?php require_once('template/footer.php'); ?>

	</body>
	</html>
