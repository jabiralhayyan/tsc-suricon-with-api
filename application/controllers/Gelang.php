<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gelang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->API = 'http://masterdata.unotech.id/api/api_bartendergelang/';
	}

	public function gelang_anak() {
		$data['title'] = 'Gelang Bayi';
		$data['query'] = '';
		$caripasien = $this->input->post('caripasien');
		if ($caripasien != '') {
			$result = $this->getPatient($caripasien);
			if(isset($result->status) && $result->status == "true") {
				$data['query'] = 'success';
				$data['no_rm'] = $result->data->no_rm;
				$data['nama'] = $result->data->nama;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
				$data['alamat'] = $result->data->alamat;
			}
		}
		//Nav
		$data['nav_ga_active'] = 'active';
		$data['nav_gd_active'] = '';
		//Tab
		$data['tab_ga_active'] = 'in active';
		$data['tab_gd_active'] = '';
		$this->load->view('gelang',$data);
	}

	public function gelang_dewasa() {
		$data['title'] = 'Gelang Dewasa';
		$data['query'] = '';
		$caripasien = $this->input->post('caripasien');
		if ($caripasien != '') {
			$result = $this->getPatient($caripasien);
			if(isset($result->status) && $result->status == "true") {
				$data['query'] = 'success';
				$data['no_rm'] = $result->data->no_rm;
				$data['nama'] = $result->data->nama;
				$data['title'] = $result->data->title;
				$data['tanggal_lahir'] = $result->data->tanggal_lahir;
				$data['alamat'] = $result->data->alamat;
			}
		}
		//Nav
		$data['nav_ga_active'] = '';
		$data['nav_gd_active'] = 'active';
		//Tab
		$data['tab_ga_active'] = '';
		$data['tab_gd_active'] = 'in active';
		$this->load->view('gelang',$data);
	}
	public function cetak_gelanganak(){
		$data['ga_rm'] = $this->input->post('ga_rm');
		$data['ga_nama'] = $this->input->post('ga_nama');
		$data['ga_title'] = $this->input->post('ga_title');
		$data['ga_ttl'] = $this->input->post('ga_ttl');
		$this->load->view('cetak_gelanganak', $data);
	}
	public function cetak_gelangdewasa(){
		$data['gd_rm'] = $this->input->post('gd_rm');
		$data['gd_nama'] = $this->input->post('gd_nama');
		$data['gd_title'] = $this->input->post('gd_title');
		$data['gd_ttl'] = $this->input->post('gd_ttl');
		$this->load->view('cetak_gelangdewasa', $data);
	}


	public function getPatient($norm){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://192.168.0.10/simrs/api/pasien/no_rm/".$norm,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Basic YWRtaW46UmFoYXNpYWJyMA==",
		    "Postman-Token: 05e949cd-acaa-40d8-a039-d548867906ec",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$result = json_decode($response);
		return $result;
	}

}