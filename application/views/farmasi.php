<?php require_once('template/header.php'); ?>

<?php
$hari_ini = str_replace('-', '', date('Y-m-d'));
function tanggal_format($tanggal) {
	if($tanggal=='' || $tanggal==NULL) return NULL;
	else {
		$split = explode('-', $tanggal);
		$tanggal = $split[2];
		$bulan = $split[1];
		$tahun = $split[0];
		$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
		return $tanggal_indo;
	}
}
function hitung_umur($tanggal, $differenceFormat = '%y')
{
	$datetime1 = date_create(date('Y-m-d'));
	$datetime2 = date_create($tanggal);
	$interval = date_diff($datetime1, $datetime2);
	return $interval->format($differenceFormat);
}
?>

<style>
.note-toolbar{
	display: none;
}
</style>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css">
<div class="container-fluid mimin-wrapper">
	<?php require_once('template/nav.php'); ?>
	<!-- start: content -->
	<div id="content">
		<div class="tabs-wrapper text-center">             
			<div class="panel box-shadow-none text-left content-header">
				<div class="panel-body" style="padding-bottom:0px;">
					<div class="col-md-12">
						<h3 class="animated fadeInLeft">Farmasi</h3>
						<p class="animated fadeInDown">
							Farmasi <span class="fa-angle-right fa"></span> Cetak
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="col-md-12 tabs-area">
								<div class="liner"></div>
								<ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo6">
									
									<!-- Etiket Obat -->
									 <li class="<?php echo $nav_et_active ;?>">
										<a href="#cetak-etiket" data-toggle="tab" title="Cetak Etiket">
											<span class="round-tabs four">
												<i class="fa fa-flask"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>ETiket Obat</b></p>
									</li>
									
									<!-- Yudidi -->
									<li class="<?php echo $nav_yd_active ;?>">
										<a href="#cetak-yudidi" data-toggle="tab" title="Cetak Yudidi">
											<span class="round-tabs four">
												<i class="fa fa-heartbeat"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Yudidi</b></p>
									</li>

									<!-- Batch -->
									<li class="<?php echo $nav_bt_active ;?>">
										<a href="#cetak-batch" data-toggle="tab" title="Cetak Batch">
											<span class="round-tabs four">
												<i class="fa fa-cart-plus"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Barcode</b></p>
									</li>

								</ul>
								<div class="tab-content tab-content-v5">
									
									<!-- FORM ETIKET -->
									<div class="tab-pane fade <?php echo $tab_et_active ;?>" id="cetak-etiket">
										<div class="col-md-12 panel-body">
											
											<form class="form-horizontal" action="<?php echo site_url(); ?>farmasi" method="POST">
												<h4><b>ETiket Obat</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Resep</label>
														<div class="col-sm-8">
															<input name="cariresep" type="text" class="form-control border-bottom" placeholder="Masukkan No Resep ...">
														</div>
														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>

											<div class="col-md-12">
												<?php if($query != ''){ ?>
												<form class="form-horizontal" id="form_obat">
													<!-- No Rekam Medis -->
													<div class="form-group"><label class="col-sm-2 control-label text-right">No RM</label>
														<div class="col-sm-8">
															<input name="et_rm" id="et_rm" type="text" class="form-control border-bottom" value="<?=$no_rm;?>" readonly>
														</div>
													</div>
													<!-- Nama -->
													<div class="form-group"><label class="col-sm-2 control-label text-right">Nama Pasien</label>
														<div class="col-sm-8">
															<input type="text" class="form-control border-bottom" value="<?=$nama;?>, <?=$title;?>" readonly>
															<input name="et_nama" id="et_nama" type="hidden" value="<?=$nama;?>">
															<input name="et_title" id="et_title" type="hidden" value="<?=$title;?>">
														</div>
													</div>
													<div class="form-group"><label class="col-sm-2 control-label text-right">Tanggal Lahir</label>
														<div class="col-sm-8">
															<input name="et_ttl" type="text" class="form-control border-bottom" value="<?=tanggal_format($tanggal_lahir);?>" readonly>
														</div>
													</div>
													<div class="form-group"><label class="col-sm-2 control-label text-right">No Resep</label>
														<div class="col-sm-8">
															<input name="et_noresep" id="et_noresep" type="text" class="form-control border-bottom" value="<?=$no_resep;?>" readonly>
														</div>
													</div>
												
													
													<table class="table table-striped table-bordered" width="100%" cellspacing="0">
														<thead>
															<tr>
																<th width="220px">Nama Obat</th>
																<th width="80px">Aturan</th>
																<th width="130px">Satuan</th>
																<th width="130px">Takaran</th>
																<th width="130px">Waktu Minum</th>
																<th width="160px">Warna Label</th>
															</tr>
														</thead>
														<tbody>
														<?php
														if($response == "true") { 
															for($i=0; $i<$jumlaharr; $i++){ 
														?> 
															<tr>
																<!-- Nama Obat -->
															<td>
																<?=$datas[$i]['nama_obat'];?> (<?=$datas[$i]['qty'];?>)
																<input type="hidden" name="et_namaobat<?=$i;?>" value="<?=$datas[$i]['nama_obat'];?>">
																<input type="hidden" name="et_jumlahobat<?=$i;?>" value="<?=$datas[$i]['qty'];?>">
															</td>
																<!-- Aturan -->
															<td>
																<?=$datas[$i]['aturan'];?>
																<input type="hidden" name="et_aturan<?=$i;?>" value="<?=$datas[$i]['aturan'];?>">
															</td>
															<!-- Satuan -->
															<td>
																<input type="text" class="form-control" name="et_satuan_manual<?=$i;?>" value="">
																<select name="et_satuan<?=$i;?>" id="et_satuan<?=$i;?>" class="select2-satuan">
																	<option></option>
																	<option>Tablet</option>
																	<option>Kapsul</option>
																	<option>Bungkus</option>
																	<option>Sirup</option>
																	<option>Drop</option>
																	<option>Tetes</option>
																	<option>Salep</option>
																</select>
															</td>
															<!-- Takaran -->
															<td>
																<input type="text" class="form-control" name="et_takaran_manual<?=$i;?>" value="">
																<select name="et_takaran<?=$i;?>" id="et_takaran<?=$i;?>" class="select2-takaran">
																	<option></option>
																	<option>Sendok Teh</option>
																	<option>Sendok Makan</option>
																	<option>Takar</option>
																	<option>Makan</option>
																	<option>Mata Kanan</option>
																	<option>Mata Kiri</option>
																	<option>Mata Kanan Kiri</option>
																	<option>Telinga Kanan</option>
																	<option>Telinga Kiri</option>
																	<option>Telinga Kanan Kiri</option>
																	<option>Hidung Kanan</option>
																	<option>Hidung Kiri</option>
																	<option>Hidung Kanan Kiri</option>
																</select>
															</td>
															<!-- Waktu Minum -->
															<td>
																<input type="text" class="form-control" name="et_waktuminum_manual<?=$i;?>" value="">
																<select class="select2-waktuminum" name="et_waktuminum<?=$i;?>" id="et_waktuminum<?=$i;?>">
																	<option></option>
																	<option>Sesudah Makan</option>
																	<option>Sebelum Makan</option>
																	<option>Saat Makan</option>
																	<option>Setiap 7 Jam</option>
																	<option>Setiap 5 Jam</option>
																	<option>Setiap 3 Jam</option>
																	<option>Setiap 2 Jam</option>
																</select>
															</td>
															<!-- Warna Label -->
															<td>
																<input type="radio" name="et_warnalabel<?=$i;?>" value="Putih" checked> Putih
																<input type="radio" name="et_warnalabel<?=$i;?>" value="Biru"> Biru
															</td>
														</tr>
														<input type="hidden" name="jumlaharr" value="<?=$jumlaharr;?>">
														<?php } }?>
														</tbody>
													</table>
													
													<center>
														<button id="putih" type="button" style="font-size: 16px;" class="btn btn-danger"><i class="fa fa-print"></i> CETAK PUTIH</button>
														
														<button id="biru" type="button" style="font-size: 16px;" class="btn btn-primary"><i class="fa fa-print"></i> CETAK BIRU</button>
													</center>

												</form>
											<?php } ?>
											</div>
										
										</div>
									</div>								


									<!-- YUDIDI -->
									<div class="tab-pane fade <?php echo $tab_yd_active ;?>" id="cetak-yudidi">
										<div class="col-md-12 panel-body">
											<form class="form-horizontal" action="<?php echo site_url(); ?>farmasi/yudidi" method="POST">
												<h4><b>Yudidi</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Pasien</label>
														<div class="col-sm-8">
															<input name="caripasien" type="text" class="form-control border-bottom" placeholder="Masukkan No Rekam Medis ...." required>
														</div>
														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>
											<table class="table table-striped table-bordered" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Nama Pasien</th>
														<th>No Rekam Medis</th>
														<th>Tanggal Lahir</th>
														<th>Waktu</th>
														<th>Tanggal Pemberian</th>
														<th>Aturan</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php if($query_yudidi != ''){ ?> 
													<form target="_blank" action="<?php echo site_url();?>farmasi/cetak_yudidi/" method="POST">
													<tr>
																<td><?=$nama;?>, <?=$title;?></td>
																<td><?=$no_rm;?></td>
																<td><?=tanggal_format($tanggal_lahir);?></td>
																
													<td>
													<input type="text" class="form-control" name="yd_waktu_manual" value="">
													<select class="form-control" name="yd_waktu" id="yd_waktu">
															<option></option>
															<option>06.00</option>
															<option>08.00</option>
															<option>12.00</option>
															<option>16.00</option>
															<option>18.00</option>
															<option>20.00</option>
															<option>24.00</option>
													</select>
												</td>
													<td><input type="date" name="yd_tglpemberian" class="form-control"></td>
												<td>
													<input type="text" class="form-control" name="yd_aturan_manual" value="">
													<select class="form-control" name="yd_aturan" id="yd_aturan">
															<option></option>
															<option>SEBELUM MAKAN</option>
															<option>SAAT MAKAN</option>
															<option>SESUDAH MAKAN</option>
													</select>
												</td>
																	<td>
																		<input type="hidden" name="yd_rm" value="<?=$no_rm;?>">
																		<input type="hidden" name="yd_nama" value="<?=$nama;?>">
																		<input type="hidden" name="yd_title" value="<?=$title;?>">
																		<input type="hidden" name="yd_ttl" value="<?=$tanggal_lahir;?>">
																		<button type="submit" class="btn btn-sm btn-primary"><span class="icons icon-printer"></span> Cetak</button>
																	</td>
																
															</tr>
															</form>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>


				<!-- BATCH -->
				<div class="tab-pane fade <?php echo $tab_bt_active ;?>" id="cetak-batch">
						<div class="col-md-12 panel-body">
							<h4><b>Barcode</b></h4>
								<form action="<?=site_url();?>farmasi/cetak_batch" target="_blank" method="POST">
										<div class="form-group">
											<label class="col-sm-2 control-label text-right">Nama Obat</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" name="bt_namaobat">
											</div>
										</div>
										<div style="padding-top: 45px"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label text-right">No Batch</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" name="bt_nobatch">
											</div>
										</div>
										<div style="padding-top: 30px"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label text-right">No Registrasi</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" name="bt_noregistrasi">
											</div>
										</div>
										<div style="padding-top: 30px"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label text-right">Tgl Produksi / MFG</label>
											<div class="col-sm-10">
												<input type="date" class="form-control" name="bt_tglproduksi">
											</div>
										</div>
										<div style="padding-top: 30px"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label text-right">Tgl Expired / EXP</label>
											<div class="col-sm-10">
												<input type="date" class="form-control" name="bt_tglexpired">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group" style="margin-top:20px !important;">
												<center>
												<button type="submit" class="btn btn-primary">
													<div>
														<span>PRINT</span>
														<span class="icons icon-printer"></span>
													</div>
												</button> 
											</center>
											</div>
										</div>
								</form>
							</div>
					</div>



													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>      
									</div>
								</div>
							</div>


							<?php require_once('template/footer.php'); ?>
	
	<script type="text/javascript">
		$(".select2-satuan").select2({
      		placeholder: "Satuan ..",
      		allowClear: true
    	});

    	$(".select2-takaran").select2({
      		placeholder: "Takaran ..",
      		allowClear: true
    	});

    	$(".select2-waktuminum").select2({
      		placeholder: "Waktu ..",
      		allowClear: true
    	});

		$(document).ready(function(){
			$('#putih').click(function(){
				var datastring = $("#form_obat").serialize();
				console.log(JSON.stringify(datastring));
				$.ajax({
					type: 'post',
					data: datastring,
					url: '<?php echo base_url() ?>farmasi/cetak_etiketputih',
					success: function (results) {
						//window.open("", "new window", "width=1024, height=768");
						var newwindow = window.open('<?php echo base_url() ?>farmasi/', '_blank');
						newwindow.document.write(results);
					}
				});
			});
			$('#biru').click(function(){
				var datastring = $("#form_obat").serialize();
				$.ajax({
					type: 'post',
					data: datastring,
					url: '<?php echo base_url() ?>farmasi/cetak_etiketbiru',
					success: function (results) {
						var newwindow = window.open("", "new window", "width=1024, height=768");
						newwindow.document.write(results);
					}
				});
			});
		});
	</script>
		
</body>
</html>
