<!-- plugins -->
<script src="<?php echo base_url();?>assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/plugins/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jquery.nicescroll.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jquery.vmap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/maps/jquery.vmap.world.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jquery.vmap.sampledata.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/chart.min.js"></script>

<script src="<?php echo base_url();?>assets/js/plugins/jquery.datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/alertify.min.js"></script>

<script src="<?php echo base_url();?>assets/js/plugins/jquery.knob.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/ion.rangeSlider.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jquery.mask.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/nouislider.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/summernote.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jquery.PrintArea.js"></script>


<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/jquery.ajaxfileupload.js"></script>

<!-- custom -->
<script src="<?php echo base_url();?>assets/js/main.js"></script>
<script>
	$('#loader').hide();
	$('.mask-money').mask('000.000.000.000.000', {reverse: true});
	$('.mask-money').mask('000.000.000.000.000', {reverse: true});
	$('.mask-date').mask('00-00-0000');
	$('.mask-date2').mask('00-00-0000');
	$('.mask-time').mask('00:00');
	$('.mask-date_time').mask('00/00/0000 00:00:00');
	$('.mask-cep').mask('00000-000');
	$('.mask-phone').mask('0000-0000');
	$('.mask-phone_with_ddd').mask('(00) 0000-0000');
	$('.mask-phone_us').mask('(000) 000-0000');
	$('.mask-mixed').mask('AAA 000-S0S');
	$('.mask-cpf').mask('000.000.000-00', {reverse: true});
	$('.mask-money').mask('000.000.000.000.000,00', {reverse: true});
	$('.mask-money2').mask("#.##0,00", {reverse: true});
	$('.mask-ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
		translation: {
			'Z': {
				pattern: /[0-9]/, optional: true
			}
		}
	});
	$('.mask-ip_address').mask('099.099.099.099');
	$('.mask-percent').mask('##0,00%', {reverse: true});
	$('.mask-clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
	$('.mask-placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
	$('.mask-fallback').mask("00r00r0000", {
		translation: {
			'r': {
				pattern: /[\/]/, 
				fallback: '/'
			}, 
			placeholder: "__/__/____"
		}
	});
	$('.mask-selectonfocus').mask("00/00/0000", {selectOnFocus: true});

	var options =  {onKeyPress: function(cep, e, field, options){
		var masks = ['00000-000', '0-00-00-00'];
		mask = (cep.length>7) ? masks[1] : masks[0];
		$('.mask-crazy_cep').mask(mask, options);
	}};

	$('.mask-crazy_cep').mask('00000-000', options);
</script>
