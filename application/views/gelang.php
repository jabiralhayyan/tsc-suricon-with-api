<?php require_once('template/header.php'); ?>

<?php
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	function hitung_umur($tanggal, $differenceFormat = '%y')
	{
    	$datetime1 = date_create(date('Y-m-d'));
    	$datetime2 = date_create($tanggal);
    	$interval = date_diff($datetime1, $datetime2);
    	return $interval->format($differenceFormat);
	}
?>

<style>
.note-toolbar{
	display: none;
}
</style>

<div class="container-fluid mimin-wrapper">
	<?php require_once('template/nav.php'); ?>
	<!-- start: content -->
	<div id="content">
		<div class="tabs-wrapper text-center">             
			<div class="panel box-shadow-none text-left content-header">
				<div class="panel-body" style="padding-bottom:0px;">
					<div class="col-md-12">
						<h3 class="animated fadeInLeft">Gelang Pasien</h3>
						<p class="animated fadeInDown">
							Gelang Pasien <span class="fa-angle-right fa"></span> Cetak
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="col-md-12 tabs-area">
								<div class="liner"></div>
								<ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo5">
									
									<!-- Gelang Anak -->
									<li class="<?php echo $nav_ga_active ;?>">
										<a href="#cetak-gelang-anak" data-toggle="tab" title="Gelang Anak">
											<span class="round-tabs four">
												<i class="fa fa-child"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Gelang Bayi</b></p>
									</li>

									<!-- Gelang Dewasa -->
									<li class="<?php echo $nav_gd_active ;?>">
										<a href="#cetak-gelang-dewasa" data-toggle="tab" title="Gelang Dewasa">
											<span class="round-tabs four">
												<i class="fa fa-male"></i>
											</span> 
										</a>
										<p style="text-align: center;margin-top: -10px;font-size: 16px; color: black"><b>Gelang Dewasa</b></p>
									</li>

								</ul>

								<div class="tab-content tab-content-v5">
									

									<!-- GELANG ANAK -->
									<div class="tab-pane fade <?php echo $tab_ga_active ;?>" id="cetak-gelang-anak">
										<div class="col-md-12 panel-body">
											<form class="form-horizontal" action="<?php echo site_url(); ?>gelang/gelang_anak" method="POST">
												<h4><b>Gelang Bayi</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Pasien</label>
														<div class="col-sm-8">
															<input name="caripasien" type="text" class="form-control border-bottom" placeholder="Masukkan No Rekam Medis ...." required>
														</div>
														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>
											<table class="table table-striped table-bordered" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Nama Pasien</th>
														<th>No Rekam Medis</th>
														<th>Alamat</th>
														<th>Tanggal Lahir</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php if($query != ''){ ?> 
														<tr>
															<td><?=$nama;?>, <?=$title;?></td>
															<td><?=$no_rm;?></td>
															<td><?=$alamat;?></td>
															<td><?=tanggal_format($tanggal_lahir);?></td>
															<td>
																<form target="_blank" action="<?php echo site_url();?>gelang/cetak_gelanganak/" method="POST">
																	<input type="hidden" name="ga_rm" value="<?=$no_rm;?>">
																	<input type="hidden" name="ga_nama" value="<?=$nama;?>">
																	<input type="hidden" name="ga_title" value="<?=$title;?>">
																	<input type="hidden" name="ga_alamat" value="<?=$alamat;?>">
																	<input type="hidden" name="ga_ttl" value="<?=$tanggal_lahir;?>">
																	<button type="submit" class="btn btn-sm btn-primary"><span class="icons icon-printer"></span> Cetak</button>
																</form>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>

									<!-- GELANG DEWASA -->
									<div class="tab-pane fade <?php echo $tab_gd_active ;?>" id="cetak-gelang-dewasa">
										<div class="col-md-12 panel-body">
											<form class="form-horizontal" action="<?php echo site_url(); ?>gelang/gelang_dewasa" method="POST">
												<h4><b>Gelang Dewasa</b></h4>
												<div class="col-md-12">
													<div class="form-group"><label class="col-sm-2 control-label text-right">Cari Pasien</label>
														<div class="col-sm-8">
															<input name="caripasien" type="text" class="form-control border-bottom" placeholder="Masukkan No Rekam Medis ...." required>
														</div>

														<div class="col-sm-2">
															<button class="btn-lg btn-success" type="submit">
																<span>CARI</span>
															</button>
														</div>
													</div>
												</div>
											</form>
											<table class="table table-striped table-bordered" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Nama Pasien</th>
														<th>No Rekam Medis</th>
														<th>Alamat</th>
														<th>Tanggal Lahir</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php if($query != ''){ ?> 
														<tr>
															<td><?=$nama;?>, <?=$title;?></td>
															<td><?=$no_rm;?></td>
															<td><?=$alamat;?></td>
															<td><?=tanggal_format($tanggal_lahir);?></td>
															<td>
																<form target="_blank" action="<?php echo site_url();?>gelang/cetak_gelangdewasa/" method="POST">
																	<input type="hidden" name="gd_rm" value="<?=$no_rm;?>">
																	<input type="hidden" name="gd_nama" value="<?=$nama;?>">
																	<input type="hidden" name="gd_title" value="<?=$title;?>">
																	<input type="hidden" name="gd_alamat" value="<?=$alamat;?>">
																	<input type="hidden" name="gd_ttl" value="<?=$tanggal_lahir;?>">
																	<button type="submit" class="btn btn-sm btn-primary"><span class="icons icon-printer"></span> Cetak</button>
																</form>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>

								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>      
				</div>
			</div>
		</div>

		
		<?php require_once('template/footer.php'); ?>

	</body>
	</html>
