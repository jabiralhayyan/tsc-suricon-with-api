<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Api_pasien extends REST_Controller {

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('Pasien');
    }

    public function index_get()
    {
        
        $id = $this->get('id');
        if($id == '') $data = $this->Pasien->get('nm_pasien, no_rkm_medis, tgl_lahir')->result();

        $this->response($data, 200);
    }

}
