<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Rawat Inap</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap2.min.css">
<script src="<?php echo base_url();?>assets/js/jquery2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap2.min.js"></script>

   <?php
	$kode = str_replace(' ', '-', $ri_rm); //untuk generate barcode
	function tanggal_format($tanggal) {
		if($tanggal=='' || $tanggal==NULL) return NULL;
		else {
			$split = explode('-', $tanggal);
			$tanggal = $split[2];
			$bulan = $split[1];
			$tahun = $split[0];
			$tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
			return $tanggal_indo;
		}
	}
	?>

<style type="text/css">
	.rotate270 {
    -webkit-transform: rotate(270deg);
    -moz-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
    transform: rotate(270deg);
}
</style>

</head>
<body>
	<div style="font-size: 11px; font-family: Arial, Helvetica, sans-serif;margin-top: 5px">
	<div style="top: 25px; right: 1px; position: absolute;">
		<!-- <img class="rotate270" alt="" src="<?php echo base_url();?>barcode/<?php echo $kode;?>.png" width="50px" height="25px"> -->
		<img class="rotate270" alt="" src="<?php echo base_url();?>barcode/<?php echo $kode;?>.png" width="50px" height="25px">
	</div>
	<table style="width: 100%;">
		<tr>
			<td><b>&nbsp;&nbsp;<?=$ri_rm;?></b></td>
		</tr>
		<tr>
			<td ><b>&nbsp;&nbsp;<?=SUBSTR($ri_nama, 0, 16);?>, <?=$ri_title;?></b></td>
		</tr>
		<tr>
			<td ><b>&nbsp;&nbsp;<?=tanggal_format($ri_ttl);?></b></td>
		</tr>
		<tr>
			<td ><b>&nbsp;&nbsp;<?=$ri_nik;?></b></td>
		</tr>
		
	</table>
</div>
</body>
</html>
<?php require_once('template/footer.php'); ?>
<!-- <button type="button" class="btn btn-primary" id="print">Print</button>
<script type="text/javascript">
	$('#print').show();
	document.querySelector("#print").addEventListener("click", function() {
		$('#print').hide();
		window.print();
		window.close();
	});
</script> -->


<!-- Langsung Cetak -- Cuman Butuh Waktu untuk generate-->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>
